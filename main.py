#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul  6 15:31:40 2019

@author: Camille
"""

#Test of Dense neural network using numpy
import numpy as np
import csv
import matplotlib.pyplot as plt

#Lets test a network of three layers : one input layer, one hidden layer and
#an output layer. We try to apply it to the hand drawn numeric caracters recognition 

from mnist import MNIST

mndata = MNIST('samples')
images, labels = mndata.load_training()

images=np.array(images)/255

w_in=np.identity(784) #Input layer weights
b_in=np.zeros((784,1)) #Input layer bias

w_h=np.random.rand(800,784)/1000 #Hidden layer weights
b_h=np.zeros((800,1)) #Hidden layer bias

w_out=np.random.rand(10,800)/1000 #Output layer weights
b_out=np.zeros((10,1)) #Output layer bias

a_in=np.zeros((784,1))
a_h = np.zeros((800,1))
a_out = np.zeros((10,1))


#Allows to visualize a caracter 
def print_greyscale(pixels, width=28, height=28):
    def get_single_greyscale(pixel):
        val = 232 + round(pixel * 23)
        return '\x1b[48;5;{}m \x1b[0m'.format(int(val))

    for l in range(height):
        line_pixels = pixels[l * width:(l+1) * width]
        print(''.join(get_single_greyscale(p) for p in line_pixels))

#Forward propagate a caracter through the network
def ForwardPropagation(im, size):
    im = np.reshape(np.transpose(im),(784,size))
    global a_in
    global a_h
    global a_out
    a_in = np.maximum(0,np.matmul(w_in,im)+b_in) #output of the input layer
    a_h = np.maximum(0,np.matmul(w_h,a_in)+b_h) #hidden layer output
    a_out = np.maximum(0,np.matmul(w_out,a_h)+b_out) #output layer
    return a_out

def VectorizedLabel(label, batch_size):
    k=0
    output = np.zeros((10,batch_size))
    for i in label:
        output[i][k]=1
        k+=1
    return output

def GradientDescent(input_img, output, ref, lamda, batch_size):
    global a_out
    global a_h
    global a_in
    ref=VectorizedLabel(ref,batch_size)
    input_img=np.reshape(input_img,(784,batch_size))
    #output layer
    d_a_out = 2*(output-ref)#gradients along neuron output
    d_b_out = np.multiply(d_a_out, output>=0)#gradient along bias
    d_w_out = np.matmul(d_b_out,np.transpose(a_h))#gradient along weights
    d_b_out = np.sum(d_b_out,axis=1)
    if(np.linalg.norm(d_w_out)!=0):
        d_w_out /= np.linalg.norm(d_w_out)
    if(np.linalg.norm(d_b_out)!=0):
        d_b_out /= np.linalg.norm(d_b_out)
    d_b_out=np.reshape(d_b_out,(10,1))
    #print(d_b_out)
    #modifying output layer parameters
    global w_out
    global b_out
    w_out -= lamda*d_w_out
    b_out -= lamda*d_b_out
    a_out = np.maximum(0,np.matmul(w_out,a_h)+b_out)
    
    
    #hidden layer
    d_a_h = np.matmul(np.transpose(w_out),np.multiply(d_a_out,a_out>=0))#gradients along neuron output
    d_b_h = np.multiply(d_a_h,a_h>=0)#gradient along bias
    d_w_h = np.matmul(d_b_h,np.transpose(a_in))#gradient along weights
    d_b_h = np.sum(d_b_h,axis=1)
    if(np.linalg.norm(d_w_h)!=0):
        d_w_h /= np.linalg.norm(d_w_h)
    if(np.linalg.norm(d_b_h)!=0):
        d_b_h /= np.linalg.norm(d_b_h)
    d_b_h=np.reshape(d_b_h,(800,1))
    #modifying hidden layer parameters
    global w_h
    global b_h
    w_h -= lamda*d_w_h
    b_h -= lamda*d_b_h
    a_h = np.maximum(0,np.matmul(w_h,a_in)+b_h)
    
    #input layer
    d_a_in = np.matmul(np.transpose(w_h),np.multiply(d_a_h,a_h>=0))#gradients along neuron output
    d_b_in = np.multiply(d_a_in,a_in>=0)#gradient along bias
    d_w_in = np.matmul(d_b_in,np.transpose(input_img))#gradient along weights
    d_b_in = np.sum(d_b_in,axis=1)
    if(np.linalg.norm(d_w_in)!=0):
        d_w_in /= np.linalg.norm(d_w_in)
    if(np.linalg.norm(d_b_in)!=0):
        d_b_in /= np.linalg.norm(d_b_in)
    d_b_in=np.reshape(d_b_in,(784,1))
    #modifying input layer parameters
    global w_in
    global b_in
    w_in -= lamda*d_w_in
    b_in -= lamda*d_b_in
    ForwardPropagation(input_img, batch_size)


def L2Error(output, ref):
    return np.trace(np.matmul(np.transpose(output-ref),output-ref))

batch_size = 50

print(L2Error(ForwardPropagation(images[:batch_size], batch_size),VectorizedLabel(labels[:batch_size], batch_size)))

#Learning parameters 
epoch =1000
step = 0.08
step_decay=0.99
error_array=[]
error_array.append(L2Error(ForwardPropagation(images[0:batch_size], batch_size),VectorizedLabel(labels[0:batch_size],batch_size)))
for i in range(epoch):
    beg = epoch*batch_size
    end = beg+batch_size
    current_result = ForwardPropagation(images[beg:end], batch_size)
    #print(current_result)
    GradientDescent(images[beg:end],current_result,labels[beg:end],step, batch_size)
    step*=step_decay
    #print(L2Error(ForwardPropagation(images[0:batch_size], batch_size),VectorizedLabel(labels[0:batch_size],batch_size)))
    error_array.append(L2Error(ForwardPropagation(images[beg:end], batch_size),VectorizedLabel(labels[beg:end],batch_size)))
for i in range(epoch):
    beg = epoch*batch_size
    end = beg+batch_size
    current_result = ForwardPropagation(images[beg:end], batch_size)
    #print(current_result)
    GradientDescent(images[beg:end],current_result,labels[beg:end],step, batch_size)
    step*=step_decay
    #print(L2Error(ForwardPropagation(images[0:batch_size], batch_size),VectorizedLabel(labels[0:batch_size],batch_size)))
    error_array.append(L2Error(ForwardPropagation(images[beg:end], batch_size),VectorizedLabel(labels[beg:end],batch_size)))
plt.plot(error_array)
print(L2Error(ForwardPropagation(images[beg:end], batch_size),VectorizedLabel(labels[beg:end],batch_size)))
acc = 0
for i in range(100):
    if(labels[i]==np.argmax(ForwardPropagation(images[i],1))):
        acc+=1
print("acc : "+str(acc))
#for i in range(10):
 #  print_greyscale(images[i], width=28, height=28)
  # print(ForwardPropagation(images[i],1))

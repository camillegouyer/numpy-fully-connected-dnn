### A Simple Implementation of a fully connected **Deep Neural Network**

This project is a python simple implementation of a fully connected three layer network only using Numpy. This implementation is meant to learn to recognize hand-drawn caracters using the _MNIST_ dataset. A classical gradient descent with learning rate decay is applied to converge to the optimal hyper-parameter state. 

#### Environment 
* Python 3.6.5 (Anaconda)
* Numpy 1.14.3
* csv 1.0
* Matplotlib 2.2.2

#### Running the Code

This project is a single main.py file. You can change the learning parameters of the network such as the number of epoch or the batch size before training the network. 

